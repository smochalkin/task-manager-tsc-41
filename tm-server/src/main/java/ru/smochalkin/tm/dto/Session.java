package ru.smochalkin.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_session")
public final class Session extends AbstractEntity implements Cloneable {

    @NotNull
    @Column(name = "user_id")
    private String userId;

    @Nullable
    private String signature;

    @Column(name = "time_stamp")
    private long timestamp = System.currentTimeMillis();

    public Session(@NotNull final String userId) {
        this.userId = userId;
    }

    @Override
    public Session clone() {
        try {
            return (Session) super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

}
