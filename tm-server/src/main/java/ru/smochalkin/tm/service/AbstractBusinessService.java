package ru.smochalkin.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.smochalkin.tm.api.IBusinessService;
import ru.smochalkin.tm.api.service.IConnectionService;
import ru.smochalkin.tm.dto.AbstractBusinessEntity;

public abstract class AbstractBusinessService<E extends AbstractBusinessEntity> extends AbstractService<E> implements IBusinessService<E> {

    public AbstractBusinessService(@NotNull IConnectionService connectionService) {
        super(connectionService);
    }

}
