package ru.smochalkin.tm.api.service;

import ru.smochalkin.tm.api.IBusinessService;
import ru.smochalkin.tm.dto.Project;

public interface IProjectService extends IBusinessService<Project> {
}
