package ru.smochalkin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.enumerated.Role;
import ru.smochalkin.tm.dto.User;

public interface IAuthService {

    @NotNull
    String getUserId();

    boolean isAuth();

    void logout();

    void login(@Nullable String login, @Nullable String password);

    void registry(@Nullable String login, @Nullable String password, @Nullable String email);

    @Nullable
    User getUser();

    void checkRoles(@Nullable Role... roles);

}
