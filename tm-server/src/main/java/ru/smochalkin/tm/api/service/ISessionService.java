package ru.smochalkin.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.api.IService;
import ru.smochalkin.tm.enumerated.Role;
import ru.smochalkin.tm.dto.Session;

import java.util.List;

public interface ISessionService extends IService<Session> {

    Session open(@NotNull String login, @NotNull String password);

    void close(@NotNull Session session);

    void closeAllByUserId(@Nullable String userId);

    void validate(@NotNull Session session);

    @SneakyThrows
    void validate(@Nullable Session session, @Nullable Role role);

    @NotNull List<Session> findAllByUserId(@Nullable String userId);

}
