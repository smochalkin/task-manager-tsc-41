package ru.smochalkin.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.smochalkin.tm.dto.Result;
import ru.smochalkin.tm.dto.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface ISessionEndpoint {
    @WebMethod
    Session openSession(@WebParam(name = "login") @NotNull String login,
                        @WebParam(name = "password") @NotNull String password);

    @WebMethod
    Result closeSession(@WebParam(name = "session") @NotNull Session session);
}
