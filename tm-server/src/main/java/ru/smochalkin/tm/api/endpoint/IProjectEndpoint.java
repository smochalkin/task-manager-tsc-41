package ru.smochalkin.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.smochalkin.tm.dto.Result;
import ru.smochalkin.tm.dto.Project;
import ru.smochalkin.tm.dto.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface IProjectEndpoint {
    @WebMethod
    @SneakyThrows
    Result createProject(
            @WebParam(name = "session") @NotNull Session session,
            @WebParam(name = "name") @NotNull String name,
            @WebParam(name = "description") @NotNull String description
    );

    @WebMethod
    @SneakyThrows
    Result changeProjectStatusById(
            @WebParam(name = "session") @NotNull Session session,
            @WebParam(name = "id") @NotNull String id,
            @WebParam(name = "status") @NotNull String status
    );

    @WebMethod
    @SneakyThrows
    Result changeProjectStatusByIndex(
            @WebParam(name = "session") @NotNull Session session,
            @WebParam(name = "index") @NotNull Integer index,
            @WebParam(name = "status") @NotNull String status
    );

    @WebMethod
    @SneakyThrows
    Result changeProjectStatusByName(
            @WebParam(name = "session") @NotNull Session session,
            @WebParam(name = "name") @NotNull String name,
            @WebParam(name = "status") @NotNull String status
    );

    @WebMethod
    @SneakyThrows
    List<Project> findProjectAllSorted(
            @WebParam(name = "session") @NotNull Session session,
            @WebParam(name = "sort") @NotNull String strSort
    );

    @WebMethod
    @SneakyThrows
    List<Project> findProjectAll(
            @WebParam(name = "session") @NotNull Session session
    );

    @WebMethod
    @SneakyThrows
    Project findProjectById(
            @WebParam(name = "session") @NotNull Session session,
            @WebParam(name = "id") @NotNull String id
    );

    @WebMethod
    @SneakyThrows
    Project findProjectByName(
            @WebParam(name = "session") @NotNull Session session,
            @WebParam(name = "name") @NotNull String name
    );

    @WebMethod
    @SneakyThrows
    Project findProjectByIndex(
            @WebParam(name = "session") @NotNull Session session,
            @WebParam(name = "index") @NotNull Integer index
    );

    @WebMethod
    @SneakyThrows
    Result removeProjectById(
            @WebParam(name = "session") @NotNull Session session,
            @WebParam(name = "id") @NotNull String id
    );

    @WebMethod
    @SneakyThrows
    Result removeProjectByName(
            @WebParam(name = "session") @NotNull Session session,
            @WebParam(name = "name") @NotNull String name
    );

    @WebMethod
    @SneakyThrows
    Result removeProjectByIndex(
            @WebParam(name = "session") @NotNull Session session,
            @WebParam(name = "index") @NotNull Integer index
    );

    @WebMethod
    @SneakyThrows
    Result clearProjects(
            @WebParam(name = "session") @NotNull Session session
    );

    @WebMethod
    @SneakyThrows
    Result updateProjectById(
            @WebParam(name = "session") @NotNull Session session,
            @WebParam(name = "id") @NotNull String id,
            @WebParam(name = "name") @NotNull String name,
            @WebParam(name = "description") @NotNull String description
    );

    @WebMethod
    @SneakyThrows
    Result updateProjectByIndex(
            @WebParam(name = "session") @NotNull Session session,
            @WebParam(name = "index") @NotNull Integer index,
            @WebParam(name = "name") @NotNull String name,
            @WebParam(name = "description") @NotNull String description
    );
}
