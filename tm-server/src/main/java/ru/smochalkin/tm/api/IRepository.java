package ru.smochalkin.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.dto.AbstractEntity;

import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    void clear();

    @NotNull
    List<E> findAll();

    @NotNull
    E findById(@Nullable String id);

    void removeById(@Nullable String id);

    int getCount();

}