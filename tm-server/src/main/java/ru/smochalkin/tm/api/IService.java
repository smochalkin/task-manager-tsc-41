package ru.smochalkin.tm.api;

import ru.smochalkin.tm.dto.AbstractEntity;

public interface IService <E extends AbstractEntity> extends IRepository<E> {
}
