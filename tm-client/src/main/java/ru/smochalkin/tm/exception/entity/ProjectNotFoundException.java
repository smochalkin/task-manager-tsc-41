package ru.smochalkin.tm.exception.entity;

import ru.smochalkin.tm.exception.AbstractException;

public final class ProjectNotFoundException extends AbstractException {

    public ProjectNotFoundException() {
        super("Error! Project not found...");
    }

}
