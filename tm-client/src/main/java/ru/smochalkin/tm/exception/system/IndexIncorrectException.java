package ru.smochalkin.tm.exception.system;

import ru.smochalkin.tm.exception.AbstractException;

public final class IndexIncorrectException extends AbstractException {

    public IndexIncorrectException(String value) {
        super("Error! `" + value + "` is not a number...");
    }

    public IndexIncorrectException() {
        super("Error! Index is incorrect...");
    }

}