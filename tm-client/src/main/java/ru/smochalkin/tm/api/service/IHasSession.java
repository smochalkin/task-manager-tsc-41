package ru.smochalkin.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.endpoint.Session;

public interface IHasSession {

    @Nullable
    Session getSession();

    void setSession(@Nullable final Session status);

}
