import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.smochalkin.tm.endpoint.*;
import ru.smochalkin.tm.marker.SoapCategory;

public class ProjectEndpointTest {

    @NotNull
    private final SessionEndpoint sessionEndpoint = new SessionEndpointService().getSessionEndpointPort();

    @NotNull
    private final ProjectEndpoint projectEndpoint = new ProjectEndpointService().getProjectEndpointPort();

    @NotNull
    private Session session;

    private int projectCount;

    @Before
    public void init() {
        session = sessionEndpoint.openSession("admin", "100");
        projectEndpoint.createProject(session, "project 1", "lorem");
        projectCount = projectEndpoint.findProjectAll(session).size();
    }

    @After
    public void after() {
        projectEndpoint.removeProjectByName(session, "project 1");
        projectEndpoint.removeProjectByName(session, "project 2");
    }

    @Test
    @Category(SoapCategory.class)
    public void createProjectTest() {
        projectEndpoint.createProject(session, "project 2", "goodbye");
        projectCount++;
        Assert.assertEquals(projectCount, projectEndpoint.findProjectAll(session).size());
    }

    @Test
    @Category(SoapCategory.class)
    public void clearProjectsTest() {
        projectEndpoint.clearProjects(session);
        Assert.assertEquals(0, projectEndpoint.findProjectAll(session).size());
    }

    @Test
    @Category(SoapCategory.class)
    public void changeProjectStatusByIdTest() {
        @NotNull final String projectId = projectEndpoint.findProjectByName(session, "project 1").getId();
        @NotNull final Result result = projectEndpoint.changeProjectStatusById(session, projectId, "COMPLETED");
        Assert.assertTrue(result.isSuccess());
    }

    @Test
    @Category(SoapCategory.class)
    public void changeProjectStatusByNameTest() {
        @NotNull final Result result = projectEndpoint
                .changeProjectStatusByName(session, "project 1", "COMPLETED");
        Assert.assertTrue(result.isSuccess());
    }

    @Test
    @Category(SoapCategory.class)
    public void changeProjectStatusByIndexTest() {
        @NotNull final Result result = projectEndpoint.changeProjectStatusByIndex(session, 0, "COMPLETED");
        Assert.assertTrue(result.isSuccess());
    }

    @Test
    @Category(SoapCategory.class)
    public void findProjectAllTest() {
        Assert.assertNotNull(projectEndpoint.findProjectAll(session));
    }

    @Test
    @Category(SoapCategory.class)
    public void findProjectAllSortedTest() {
        Assert.assertNotNull(projectEndpoint.findProjectAllSorted(session, "NAME"));
    }

    @Test
    @Category(SoapCategory.class)
    public void findProjectByIdTest() {
        @NotNull final String projectId = projectEndpoint.findProjectByName(session, "project 1").getId();
        @NotNull final Project project = projectEndpoint.findProjectById(session, projectId);
        Assert.assertEquals("project 1", project.getName());
    }

    @Test
    @Category(SoapCategory.class)
    public void findProjectByNameTest() {
        @NotNull final Project project = projectEndpoint.findProjectByName(session, "project 1");
        Assert.assertEquals("lorem", project.getDescription());
    }

    @Test
    @Category(SoapCategory.class)
    public void findProjectByIndexTest() {
        @NotNull final Project project = projectEndpoint.findProjectByIndex(session, 0);
        Assert.assertEquals("project 1", project.getName());
    }

    @Test
    @Category(SoapCategory.class)
    public void removeProjectByIdTest() {
        @NotNull final String projectId = projectEndpoint.findProjectByName(session, "project 1").getId();
        @NotNull final Result result = projectEndpoint.removeProjectById(session, projectId);
        Assert.assertTrue(result.isSuccess());
    }

    @Test
    @Category(SoapCategory.class)
    public void removeProjectByNameTest() {
        @NotNull final Result result = projectEndpoint.removeProjectByName(session, "project 1");
        Assert.assertTrue(result.isSuccess());
    }

    @Test
    @Category(SoapCategory.class)
    public void removeProjectByIndexTest() {
        @NotNull final Result result = projectEndpoint.removeProjectByIndex(session, 0);
        Assert.assertTrue(result.isSuccess());
    }

    @Test
    @Category(SoapCategory.class)
    public void updateProjectByIdTest() {
        @NotNull final String projectId = projectEndpoint.findProjectByName(session, "project 1").getId();
        projectEndpoint.updateProjectById(session, projectId, "new name", "new desc");
        @NotNull final Project project = projectEndpoint.findProjectById(session, projectId);
        Assert.assertEquals("new name", project.getName());
        Assert.assertEquals("new desc", project.getDescription());
    }

    @Test
    @Category(SoapCategory.class)
    public void updateProjectByIndexTest() {
        projectEndpoint.updateProjectByIndex(session, 0, "new name", "new desc");
        @NotNull final Project project = projectEndpoint.findProjectByIndex(session, 0);
        Assert.assertEquals("new name", project.getName());
        Assert.assertEquals("new desc", project.getDescription());
    }

}
